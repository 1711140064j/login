Rails.application.routes.draw do
    get 'tops/main'
    post 'tops/login'
    root 'tops#main'
    get 'tops/logout'
    resources :users, only: [:index, :new, :create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
